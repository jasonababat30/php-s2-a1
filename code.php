<?php
	// Array Manipulation

	/*
		Activity Instruction:

		1. Create a function called 'createProduct' that accepts a name of the product and price as its parameter and add it to an array called 'products'.

		2. Create a function called 'printProducts' that displays the lists of elements of products array on an unordered list in the 'index.php'.

		3. Create a function called 'countProducts' that displays the count of the elements on a products array in the 'index.php'.

		4. Create a function called 'deleteProduct' that deletes an element to a products array
	*/

	$products = [];

	//No. 1

	/*
		My answer:

		function createProduct($productName, $productPrice){

			$array[$productName] = $productPrice;

			return $array;
		}

		$products = createProduct($products, "Laptop", 50000);
	*/

	//Instructor's Answer:

	function createProduct($name, $price){

		global $products;

		array_push($products, ['name' => $name, 'price' => $price]);

	}

	//No. 2

	function printProducts($array){
		
		foreach($array as $element){
			echo "<li>";
			foreach($element as $key => $value){
				echo "$key : $value";
			}
			echo "</li>";
		}

	}

	//No. 3

	function countProducts($array){
		echo count($array);
	}

	//No. 4
	function deleteProduct(){
		global $products;
		array_pop($products);
	}

?>