<?php require "./code.php" ?>
<!DOCTYPE html>
<html>
<head>
	<title>ACTIVITY - PHP Array Manipulation</title>
</head>
<body>
	<h1>ACTIVITY - PHP Array Manipulation</h1>

	<pre>
		<?php createProduct('Laptop', 25000.00); ?>
		<?php createProduct('Gameboy', 10000.00); ?>
		<?php createProduct('USB', 5000.00); ?>
		<?php print_r($products); ?>
	</pre>

	<ul>
		<h2>Products List:</h2>
		<?php printProducts($products); ?>
	</ul>

	<pre>
		<?php countProducts($products); ?>
	</pre>

	<pre>
		<?php deleteProduct(); ?>
		<?php print_r($products); ?>
	</pre>
</body>
</html>